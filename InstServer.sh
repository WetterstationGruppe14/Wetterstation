

echo installiere Server

apt-get update

apt-get install curl

curl -sL https://deb.nodesource.com/setup_9.x -o nodesource_setup.sh  # 
																	  #
apt-get install -y nodejs											  # funktioniert nicht in VM(falsche version wird installiert)

apt-get install build-essential

apt-get install npm

npm install ejs

npm install express

npm install mysql

npm install ws

npm install openweather-apis

npm install openweather-apis --save
 
npm install -g openweather-apis



apt-get install mysql-server


mysql -u root -p <<EOF


SET time_zone = '+0:00';
SET GLOBAL time_zone = '+0:00';


CREATE DATABASE Wetterstation;

use Wetterstation;

CREATE TABLE Wetterstation.Daten(Außentemperatur Float, Innentemperatur Float, Luftfeuchtigkeit int, Luftdruck int, Datum DATETIME Primary Key);

CREATE TABLE Wetterstation.Benutzerdaten(id int Primary Key, E_Mail_Addr varchar(255), E_Mail_Zeit Time, Message varChar(255));

CREATE TABLE Wetterstation.Controll(Messzeit_Intervall int, Alarm_Aussen_Temp int);

CREATE TABLE Wetterstation.Log(Log varchar(255), Datum DATETIME Primary Key);    

CREATE TABLE Wetterstation.Aussentemperatur(Aussentemperatur FLOAT);


Insert into Benutzerdaten values(1,'exmpl@web.de', '12:12:34', 'Hallo World');

Insert into Controll values(5,0);

insert into Aussentemperatur values(0.0);



EOF
