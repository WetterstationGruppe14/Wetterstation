/**
  ***************************
  * Projekt iSystem2017
  * Datei   Eisntellungen.js
  * Author  Fabian Schoch
  * Version V1.0
  * Datum   11.03.2018
  ***************************
*/


//Verbindung zu Websocket Aufbauen
var ws = new WebSocket("ws://192.168.42.128:8081");	



///////////////////////////////////////Events///////////////////////////////////////


ws.onopen = function(){							//wenn verbindung zu socket aufgebaut
	document.getElementById("ws_status").innerHTML="Connected";
};

ws.onclose = function(){						//wenn verbindung zu socket abgebrochen
	document.getElementById("ws_status").innerHTML="DISCONECTED";
};

ws.onmessage = function(payload){				//Wenn daten von Server rufe printMessage(...) auf
	Message_from_Server(payload.data);
};




/*
Aufgabe:	-Beim drücken des Button Speichern sende alle Daten der input-Feldern an Server
Parameter: 	-keine
Rückgabe:  	-keine
*/

function Speichern(){


	var msg = {"ID":"Einstellungen", "Daten":[]}


	msg.Daten = {	"E_Mail_Addr": document.querySelector("#E_Mail_Addr").value,
					"E_Mail_Zeit": document.querySelector("#E_Mail_Zeit").value,
					"Message": document.querySelector("#Message").value,
					"Messzeit_Intervall":document.querySelector("#Messzeit_Intervall").value,
					"Alarm_Außen_Temp":document.querySelector("#Alarm_Außen_Temp").value}

	Send_to_Server(msg);
}

///////////////////////////////////////Functions///////////////////////////////////////////




/*
Aufgabe:	-Bekommt Daten von Server und setzt diese in die jeweiligen in HTML Elemente
Parameter: 	-Nachricht von Server
Rückgabe:  	-keine
*/


function Send_to_Server(daten){

	var S_Daten = JSON.stringify(daten);
	ws.send(S_Daten);
}


/*
Aufgabe:	-Bekommt Daten von Server und setzt diese in die jeweiligen in HTML Elemente
Parameter: 	-Nachricht von Server
Rückgabe:  	-keine
*/

function Message_from_Server(msg) {				
	
	var message = JSON.parse(msg);

	if(message.ID=="Benutzerdaten")	{
		document.getElementById("E_Mail_Addr").value=message.Daten.E_Mail_Addr;
		document.getElementById("E_Mail_Zeit").value=message.Daten.E_Mail_Zeit;
		document.getElementById("Message").value=message.Daten.Message;
		document.getElementById("Messzeit_Intervall").value=message.Daten.Messzeit_Intervall;
		document.getElementById("Alarm_Außen_Temp").value=message.Daten.Alarm_Außen_Temp;

	}





	if(message.ID=="Eingabefehler")	{

		alert(message.Daten);
		location.reload();
	}

	/*if(message.ID=="Eingabegespeichert")	{

		alert("Eingabe Erfolgreich");
	}*/


	if(message.ID=="Datum"){document.getElementById("Datum").innerHTML=message.Daten;}


}















