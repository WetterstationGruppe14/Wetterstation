/**
  ***************************
  * Projekt iSystem2017
  * Datei   Log.js
  * Author  Fabian Schoch
  * Version V1.0
  * Datum   11.03.2018
  ***************************
*/


//Verbindung zu Websocket Aufbauen
var ws = new WebSocket("ws://192.168.42.128:8081");	




///////////////////////////////////////Events///////////////////////////////////////


ws.onopen = function(){							//wenn verbindung zu socket aufgebaut
	document.getElementById("ws_status").innerHTML="Connected";
};

ws.onclose = function(){						//wenn verbindung zu socket abgebrochen
	document.getElementById("ws_status").innerHTML="DISCONECTED";
};

ws.onmessage = function(payload){				//Wenn daten von Server rufe printMessage(...) auf
	Message_from_Server(payload.data);
};


///////////////////////////////////////Functions///////////////////////////////////////////




/*
Aufgabe:	-Bekommt Daten von Server und setzt diese in die jeweiligen in HTML Elemente
Parameter: 	-Nachricht von Server
Rückgabe:  	-keine
*/


function Send_to_Server(daten){

	var S_Daten = JSON.stringify(daten);
	ws.send(S_Daten);
}



/*
Aufgabe:	-Bekommt Daten von Server und setzt diese in die jeweiligen in HTML Elemente
Parameter: 	-Nachricht von Server
Rückgabe:  	-keine
*/

function Message_from_Server(msg) {				
	
	var message = JSON.parse(msg);


	if(message.ID=="DeletLog"){document.querySelector('div.Log').innerHTML="";}
	if(message.ID=="Log"){
		var p = document.createElement('p');
		p.innerText = message.Daten;
		document.querySelector('div.Log').appendChild(p);	
	}
	if(message.ID=="Datum"){document.getElementById("Datum").innerHTML=message.Daten;}
}















