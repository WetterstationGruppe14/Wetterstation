/**
  ***************************
  * Projekt iSystem2017
  * Datei   Verlauf.js
  * Author  Fabian Schoch
  * Version V1.0
  * Datum   11.03.2018
  ***************************
*/


//Verbindung zu Websocket Aufbauen
var ws = new WebSocket("ws://192.168.42.128:8081");	





///////////////////////////////////////Events///////////////////////////////////////


ws.onopen = function(){							//wenn verbindung zu socket aufgebaut
	document.getElementById("ws_status").innerHTML="Connected";
};

ws.onclose = function(){						//wenn verbindung zu socket abgebrochen
	document.getElementById("ws_status").innerHTML="DISCONECTED";
};

ws.onmessage = function(payload){				//Wenn daten von Server rufe printMessage(...) auf
	Message_from_Server(payload.data);
};


///////////////////////////////////////Functions///////////////////////////////////////////




/*
Aufgabe:	-Sendet Daten an Server
Parameter: 	-Nachricht an Server
Rückgabe:  	-keine
*/

function Send_to_Server(daten){

	var S_Daten = JSON.stringify(daten);
	ws.send(S_Daten);
}





/*
Aufgabe:	-Bekommt Daten von Server und setzt diese in die jeweiligen in HTML Elemente
Parameter: 	-Nachricht von Server
Rückgabe:  	-keine
*/

function Message_from_Server(msg) {				
	
	var message = JSON.parse(msg);
/*
	if(message.ID=="Verlauf"){
		var p = document.createElement('p');
		p.innerText = 	Runde(message.Daten.Innentemperatur,1) + " " + 
						Runde(message.Daten.Außentemperatur,1) +" " +
						Runde(message.Daten.Luftdruck)+" " + 
				     	Runde(message.Daten.Luftfeuchtigkeit,1)+" " + 
						message.Daten.Datum;

		document.querySelector('div.Verlauf').appendChild(p);	
	}	
*/

	if(message.ID=="Verlauf"){
		
		var tr= document.createElement('tr');
		tr.innerHTML= '<td>' +Runde(message.Daten.Innentemperatur,1)+"°C" + '</td>';
		tr.innerHTML+= "<td>" +Runde(message.Daten.Außentemperatur,1)+"°C" + "</td>";
		tr.innerHTML+= "<td>" +Runde(message.Daten.Luftdruck,1)+"hPa" + "</td>";
		tr.innerHTML+= "<td>" +Runde(message.Daten.Luftfeuchtigkeit,1)+"%" + "</td>";
		tr.innerHTML+= "<td>" +message.Daten.Datum+ "</td>";

		document.querySelector('tbody').appendChild(tr);
		
	}








	if(message.ID=="Datum"){document.getElementById("Datum").innerHTML=message.Daten;}
}








/*
Aufgabe:	-Runden auf x Stellen
Parameter: 	-Wert: Zu rundenden Wert
			-Nachkommastellen: Auf wie viel stellen gerundet werden soll
Rückgabe:  	-Gerundeter wert;
*/

function Runde(Wert,Nachkommastelle){

	if(!Nachkommastelle) Nachkommastelle = 0;
	return Math.round(Wert * Math.pow(10,Nachkommastelle))/Math.pow(10,Nachkommastelle);
}







