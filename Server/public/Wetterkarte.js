/**
  ***************************
  * Projekt iSystem2017
  * Datei   Wetterkarte.js
  * Author  Fabian Schoch
  * Version V0.1
  * Datum   21.01.2018
  ***************************
*/


//Verbindung zu Websocket Aufbauen
var ws = new WebSocket("ws://192.168.42.128:8081");	





///////////////////////////////////////Events///////////////////////////////////////


ws.onopen = function(){							//wenn verbindung zu socket aufgebaut
	document.getElementById("ws_status").innerHTML="Connected";
};

ws.onclose = function(){						//wenn verbindung zu socket abgebrochen
	document.getElementById("ws_status").innerHTML="DISCONECTED";
};

ws.onmessage = function(payload){				//Wenn daten von Server rufe printMessage(...) auf
	Message_from_Server(payload.data);
};


///////////////////////////////////////Functions///////////////////////////////////////////




/*
Aufgabe:	-Bekommt Daten von Server und setzt diese in die jeweiligen in HTML Elemente
Parameter: 	-Nachricht von Server
Rückgabe:  	-keine
*/


function Send_to_Server(daten){

	var S_Daten = JSON.stringify(daten);
	ws.send(S_Daten);
}



/*
Aufgabe:	-Bekommt Daten von Server und setzt diese in die jeweiligen in HTML Elemente
Parameter: 	-Nachricht von Server
Rückgabe:  	-keine
*/

function Message_from_Server(msg) {				
	
	var message = JSON.parse(msg);

	if(message.ID=="Datum"){document.getElementById("Datum").innerHTML=message.Daten;}
}















