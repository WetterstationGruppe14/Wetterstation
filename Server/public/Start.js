/**
  ***************************
  * Projekt iSystem2017
  * Datei   Start.js
  * Author  Fabian Schoch
  * Version V1.0
  * Datum   11.03.2018
  ***************************
*/


//Verbindung zu Websocket Aufbauen
var ws = new WebSocket("ws://192.168.42.128:8081");	





///////////////////////////////////////Events///////////////////////////////////////


ws.onopen = function(){							//wenn verbindung zu socket aufgebaut
	document.getElementById("ws_status").innerHTML="Connected";
};

ws.onclose = function(){						//wenn verbindung zu socket abgebrochen
	document.getElementById("ws_status").innerHTML="DISCONECTED";
};

ws.onmessage = function(payload){				//Wenn daten von Server rufe printMessage(...) auf
	Message_from_Server(payload.data);
};



///////////////////////////////////////Functions///////////////////////////////////////////



/*
Aufgabe:	-Bekommt Daten von Server und setzt diese in die jeweiligen in HTML Elemente
Parameter: 	-Nachricht von Server
Rückgabe:  	-keine
*/

function Send_to_Server(daten){

	var S_Daten = JSON.stringify(daten);
	ws.send(S_Daten);
}





/*
Aufgabe:	-Bekommt Daten von Server und setzt diese in die jeweiligen in HTML Elemente
Parameter: 	-Nachricht von Server
Rückgabe:  	-keine
*/

function Message_from_Server(msg) {				
	
	var message = JSON.parse(msg);


	if(message.ID=="EchtzeitDaten") {

		document.getElementById("Außentemperatur").innerHTML	= message.Daten.Außentemperatur +"°C";
		document.getElementById("Innentemperatur").innerHTML	= message.Daten.Innentemperatur +"°C";
		document.getElementById("Luftfeuchtigkeit").innerHTML	= message.Daten.Luftfeuchtigkeit +"%";
		document.getElementById("Luftdruck").innerHTML			= message.Daten.Luftdruck +" hPa";
	}


	if(message.ID=="AußentemperaturMIN")   	{document.getElementById("AußentemperaturMIN").innerHTML=Runde(message.Daten,1) +"°C";}
	if(message.ID=="AußentemperaturAVG")   	{document.getElementById("AußentemperaturAVG").innerHTML=Runde(message.Daten,1) +"°C";}
	if(message.ID=="AußentemperaturMAX")   	{document.getElementById("AußentemperaturMAX").innerHTML=Runde(message.Daten,1) +"°C";}

	if(message.ID=="InnentemperaturMIN")   	{document.getElementById("InnentemperaturMIN").innerHTML=Runde(message.Daten,1) +"°C";}
	if(message.ID=="InnentemperaturAVG")   	{document.getElementById("InnentemperaturAVG").innerHTML=Runde(message.Daten,1) +"°C";}
	if(message.ID=="InnentemperaturMAX")   	{document.getElementById("InnentemperaturMAX").innerHTML=Runde(message.Daten,1) +"°C";}

	if(message.ID=="LuftfeuchtigkeitMIN")   {document.getElementById("LuftfeuchtigkeitMIN").innerHTML=Runde(message.Daten,1) +"%";}
	if(message.ID=="LuftfeuchtigkeitAVG")   {document.getElementById("LuftfeuchtigkeitAVG").innerHTML=Runde(message.Daten,1) +"%";}
	if(message.ID=="LuftfeuchtigkeitMAX")   {document.getElementById("LuftfeuchtigkeitMAX").innerHTML=Runde(message.Daten,1) +"%";}

	if(message.ID=="LuftdruckMIN")   		{document.getElementById("LuftdruckMIN").innerHTML=Runde(message.Daten) +" hPa";}
	if(message.ID=="LuftdruckAVG")  		{document.getElementById("LuftdruckAVG").innerHTML=Runde(message.Daten) +" hPa";}
	if(message.ID=="LuftdruckMAX")  		{document.getElementById("LuftdruckMAX").innerHTML=Runde(message.Daten) +" hPa";}


	if(message.ID=="Datum"){document.getElementById("Datum").innerHTML=message.Daten;}
}


/*
Aufgabe:	-Runden auf x Stellen
Parameter: 	-Wert: Zu rundenden Wert
			-Nachkommastellen: Auf wie viel stellen gerundet werden soll
Rückgabe:  	-Gerundeter wert;
*/

function Runde(Wert,Nachkommastelle){

	if(!Nachkommastelle) Nachkommastelle = 0;
	return Math.round(Wert * Math.pow(10,Nachkommastelle))/Math.pow(10,Nachkommastelle);
}














