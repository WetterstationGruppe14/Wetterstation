/**
  ***************************
  * Projekt iSystem2017
  * Datei   Server.JS
  * Author  Fabian Schoch
  * Version V0.1
  * Datum   21.01.2018
  ***************************
*/



//////////////////////Header/////////////////////

//Framework "ws" (websocket) verwenden
var WebSocketServer = require("ws").Server;
//Websocket auf Port 8081 
var wss = new WebSocketServer({port : 8081});
//Clientliste
var CLIENTS = [];



//Framework "mysql" verwenden
var mysql = require('mysql'); 



//Framework "express" verwenden
var express = require('express');
var app = express();



// Außentemperatur von Openwerther
var weather = require('openweather-apis');
// Einstellungen für Außentemperatur
weather.setLang('de');
weather.setCity('Mosbach');
weather.setCityId(2869120);
weather.setUnits('metric');
weather.setAPPID('f199a88cfd8c2da31751bf8c6626b8d4');
Write_Außentemperatur_inDB();



//Enthält aktuelle geöffene Webseite
var Site="";


var init = false;

/////////////////////Conection/////////////////////

//Bekanntmachung des Ordners public Inhalt: Style.css und client.js
app.use(express.static('public'))						//bekannt machen des public ordner mit client.js datei darin


/*
Aufgabe:	-Laden der Verschiedenen Seiten
*/

app.get("/", function(req,res){							//html einbinden
    //res.send("hallo welt");
	res.render("Start.ejs",{});
	Site="Start"
})


app.get("/Wetterkarte", function(req,res){							
    //res.send("hallo welt");
	res.render("Wetterkarte.ejs",{});
	Site="Wetterkarte"
})


app.get("/Verlauf", function(req,res){							
    //res.send("hallo welt");
	res.render("Verlauf.ejs",{});
	Site="Verlauf"
})

app.get("/Einstellungen", function(req,res){			
    //res.send("hallo welt");
	res.render("Einstellungen.ejs",{});
	Site="Einstellungen"	
})

app.get("/Log", function(req,res){							
    //res.send("hallo welt");
	res.render("Log.ejs",{});
	Site="Log"
})



/*
Aufgabe:	-Server Starten mit Eigener IP addresse und Port 8080 
*/

var server = app.listen(8080,'0.0.0.0',function() {		//Server starten
    console.log("Server runs");
});


/*
Aufgabe:	-Bei Verbindungsaufbau Lognachricht und jeweilige Daten Senden
*/

wss.on("connection", function(ws){						

	CLIENTS.push(ws);

	console.log("WebSocket connected to client");
	
	if(Site=="Einstellungen"){Send_Einstellungen();}
	if(Site=="Log"){Send_Log();}
	if(Site=="Verlauf"){Send_Verlauf();}
	if(Site=="Start"){Send_Daten();}
	Send_Datum();

	/*
	Aufgabe:	-Bei nachricht von Client leite diese weiter an Funktion "Message_from_Client"
	*/
	ws.on('message', function(msg){						
		Message_from_Client(msg);
	});

});


/*
Aufgabe:	-Client aus liste werfen beim schließen
*/

wss.on('close', function() {
    console.log('Client disconnected.');
	CLIENTS.splice(CLIENTS.indexOf(wss), 1)

});


/*
Aufgabe:	-Client aus liste werfen bei Fehler
*/

wss.on('error', function() {
    console.log('ERROR');
	CLIENTS.splice(CLIENTS.indexOf(wss), 1)

});



/*
Aufgabe:	-Stellt Datenbank verbindung her
*/

var con = mysql.createConnection({						
	host: "localhost",
	user: "root",
	password: "123456",
	database: "Wetterstation"
});


/*
Aufgabe:	-Lognachricht wenn DB verbidung aufgebaut
*/

con.connect(function(err) {								
	if (err){ throw err;
	console.log(err);}
	console.log("Connected to db ");
});





//////////////////////Main/////////////////////








/*
Aufgabe:	Außentemperatur alle 10min von http://openweathermap.org/ holen
*/
setInterval(function(){Write_Außentemperatur_inDB()}, 600000);

function Write_Außentemperatur_inDB(){
    weather.getTemperature(function(err, temp){
        console.log(temp);
		con.query("UPDATE Aussentemperatur SET Aussentemperatur = '"+ temp +"'", function  (err, rows){
			if (err){Send_to_Client({"ID":"Eingabefehler", "Daten":"Ungültige Eingabe"});}
		});
    });
}




/*
Aufgabe:	-Wertet Nachricht von Client aus und Speichert die Daten in entsprechenden DB Spalte
Parameter: 	-Nachricht von Client
Rückgabe:  	-keine
*/

function Message_from_Client(msg){
	
	var message = JSON.parse(msg);						
	if(message.ID == "Einstellungen"){

		Set_E_Mail_Addr_in_DB(message.Daten.E_Mail_Addr);
		Set_E_Mail_Zeit_in_DB(message.Daten.E_Mail_Zeit);
		Set_Message_in_DB(message.Daten.Message);
		Set_Messzeit_Intervall_in_DB(message.Daten.Messzeit_Intervall);
		Set_Alarm_Außen_Temp_in_DB(message.Daten.Alarm_Außen_Temp);
	}

}






/*
Aufgabe:	-Schreibt Emailaddrese in DB
Parameter: 	-Emailaddrese
Rückgabe:  	-keine
*/

function Set_E_Mail_Addr_in_DB(E_Mail_Addr){
	
	var error = false; 

	if( String(E_Mail_Addr).search("@") != -1 && String(E_Mail_Addr).search(/[.]/g) != -1){

		con.query("UPDATE Benutzerdaten SET E_Mail_Addr = '"+ E_Mail_Addr +"'", function (err, rows){
			if (err){Send_to_Client({"ID":"Eingabefehler", "Daten":"Ungültige Eingabe"});}
		});
	}
	else{
		Send_to_Client({"ID":"Eingabefehler", "Daten":"Ungültige Eingabe Email Adresse"});

	}
	
			
}

/*
Aufgabe:	-Schreibt Sendezeit für Email in DB
Parameter: 	-Sendezeit
Rückgabe:  	-keine
*/

function Set_E_Mail_Zeit_in_DB(E_Mail_Zeit){
	

	con.query("UPDATE Benutzerdaten SET E_Mail_Zeit = '"+ E_Mail_Zeit +"'", function (err, rows){
		if (err){Send_to_Client({"ID":"Eingabefehler", "Daten":"Ungültige Eingabe(Falsches Format)"});}
	});
	
}

/*
Aufgabe:	-Schreibt Nachricht für SensHat in DB
Parameter: 	-Nachricht
Rückgabe:  	-keine
*/

function Set_Message_in_DB(Message){

	con.query("UPDATE Benutzerdaten SET Message = '"+ Message +"'", function (err, rows){
		if (err){Send_to_Client({"ID":"Eingabefehler", "Daten":"Ungültige Eingabe"});}
	});

}

/*
Aufgabe:	-Schreibt Messzeit Intervall in DB
Parameter: 	-Messzeit Intervall
Rückgabe:  	-keine
*/

function Set_Messzeit_Intervall_in_DB(Messzeit_Intervall){

	if(Messzeit_Intervall >=1 && Messzeit_Intervall <= 21600 ){
		con.query("UPDATE Controll SET Messzeit_Intervall = '"+ Messzeit_Intervall +"'", function  (err, rows){
			if (err){Send_to_Client({"ID":"Eingabefehler", "Daten":"Ungültige Eingabe"});}
		});
	}
	else{
		Send_to_Client({"ID":"Eingabefehler", "Daten":"Ungültige Eingabe(1s < Intervall < 21600s)"});
	}
}


/*
Aufgabe:	-Schreibt Allarmschwellwert der Außentemperatur in DB
Parameter: 	-Schwellwert
Rückgabe:  	-keine
*/

function Set_Alarm_Außen_Temp_in_DB(Alarm_Außen_Temp){
		
	if(Alarm_Außen_Temp >=-40 && Alarm_Außen_Temp <= 40 ){
		con.query("UPDATE Controll SET Alarm_Aussen_Temp = '"+ Alarm_Außen_Temp +"'", function  (err, rows){
			if (err){Send_to_Client({"ID":"Eingabefehler", "Daten":"Ungültige Eingabe"});}
		});
	}
	else{
		Send_to_Client({"ID":"Eingabefehler", "Daten":"Ungültige Eingabe(-40 < Alarm < 40)"});

	}
}




/*
Aufgabe:	-Holt Wetterdaten der Letzten 24H aus der Datenbank und Sendet diese an Client (Wird aufgerufen beim Laden der Seite Wettervarlauf)
Parameter: 	-Keine
Rückgabe:  	-keine
*/

function Send_Verlauf(){

	con.query("SELECT * FROM Daten", function (err, result, fields) {
	var msg = {"ID":"Verlauf", "Daten":[]}

		if (err) console.log(err);
		else{
			for(var i = result.length-1; i>=0 ; i--){
				
 				var d = new Date(result[i].Datum);
				var Datum = { "Jahr":"", "Monat":"", "Tag":"", "Stunden":"", "Minuten":"", "Sekunden":"" };
			
				Datum.Jahr = d.getYear()+1900;
				Datum.Monat = d.getMonth()+1;
				Datum.Tag = d.getDate();
				Datum.Sekunden = d.getSeconds();
				Datum.Stunden = d.getHours();
				Datum.Minuten = d.getMinutes();


				Datum = FormateDatum(Datum);
				msg.Daten = {	"Außentemperatur": result[i].Außentemperatur,
							  	"Innentemperatur": result[i].Innentemperatur, 
								"Luftfeuchtigkeit": result[i].Luftfeuchtigkeit,
							  	"Luftdruck":result[i].Luftdruck, 
								"Datum": Datum.Tag +"." + Datum.Monat + "." + Datum.Jahr+" "+ Datum.Stunden+":"+Datum.Minuten+":"+Datum.Sekunden }
				Send_to_Client(msg);
			}
		}
	});
}



/*
Aufgabe:	-Holt Einstellungen aus der Datenbank und Sendet diese an Client (Wird aufgerufen beim Laden der Seite Einstellungen)
Parameter: 	-Keine
Rückgabe:  	-keine
*/

function Send_Einstellungen(){
		

	var msg = {"ID":"Benutzerdaten", "Daten":[]}
	var buffer;

	con.query("SELECT * FROM Benutzerdaten", function (err, result, fields){
		if (err) throw err;
		buffer = result;
	});

	con.query("SELECT * FROM Controll", function (err, result, fields){
		if (err) throw err;
		
		msg.Daten ={	"Messzeit_Intervall":result[0].Messzeit_Intervall,
						"Alarm_Außen_Temp":result[0].Alarm_Aussen_Temp,
						"E_Mail_Addr":buffer[0].E_Mail_Addr,
						"E_Mail_Zeit":buffer[0].E_Mail_Zeit,
						"Message":buffer[0].Message  };
		Send_to_Client(msg);
	});

}



/*
Aufgabe:	-Holt Log aus der Datenbank und Sendet diese an Client (Wird aufgerufen beim Laden der Seite Log)
Parameter: 	-Keine
Rückgabe:  	-keine
*/

function Send_Log(){

	con.query("SELECT * FROM Log", function (err, result, fields) {
		if (err) throw err;
		//console.log(result);

		Send_to_Client({"ID":"DeletLog", "Daten":""});

		for(var i = result.length-1; i>=0 ; i--){

			Send_to_Client({"ID":"Log", "Daten":result[i].Log +"\n"+ result[i].Datum});
		}
	});
}





/*
Aufgabe:	-Ruft alle 1000ms funktion Send_Daten auf
Parameter: 	-Keine
Rückgabe:  	-keine
*/

setInterval(function(){ Send_Daten() }, 1000);



/*
Aufgabe:	-Sendet alle 1000ms alle "Echtzeit" Daten an Client 
Parameter: 	-Keine
Rückgabe:  	-keine
*/

function Send_Daten(){
	
	
	try{
		Send_Datum();
		AktuelleWetterDaten_Senden();
		AVGMINMAX_Senden();
	
	}

	catch(ex){ console.log("Fehler beim senden/DB zugriff");}
	
}


/*
Aufgabe:	-Sendet aktuelles Datum an Client
Parameter: 	-Keine
Rückgabe:  	-keine
*/

function Send_Datum(){

	var Datum = AktuellesDatum();
	Datum = Datum.Tag+"."+Datum.Monat+"."+Datum.Jahr+" "+Datum.Stunden+":"+Datum.Minuten+":"+Datum.Sekunden;
	Send_to_Client({"ID":"Datum", "Daten":Datum});
}



/*
Aufgabe:	-Holt aktuelle Wetterdaten aus DB und sended diese an Client
Parameter: 	-Keine
Rückgabe:  	-keine
*/

function AktuelleWetterDaten_Senden(){

	con.query("SELECT * FROM Daten", function (err, result, fields) {
		if (err) console.log(err);

		var LastData = result.length-1;

		var msg = {"ID":"EchtzeitDaten", "Daten":[]}
		
		msg.Daten = {	"Außentemperatur":result[LastData].Außentemperatur,
						"Innentemperatur":result[LastData].Innentemperatur,
						"Luftfeuchtigkeit":result[LastData].Luftfeuchtigkeit,
						"Luftdruck":result[LastData].Luftdruck };

		Send_to_Client(msg);
	});
}



/*
Aufgabe:	-Sendet alle MIN,MAX und AVG Wetterdaten an Client
Parameter: 	-Keine
Rückgabe:  	-keine
*/

function AVGMINMAX_Senden(){

	var Datum = AktuellesDatum();
	

	var TagMinus1= Datum.Tag-1;
	if (TagMinus1<10){TagMinus1= "0" + TagMinus1;}
	
	var Datum24H_Früher = Datum.Jahr + Datum.Monat + TagMinus1 + Datum.Stunden + Datum.Minuten + Datum.Sekunden;


	GetMINMAXAVG_Daten("min","Außentemperatur",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"AußentemperaturMIN", "Daten":data});
    });
	GetMINMAXAVG_Daten("avg","Außentemperatur",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"AußentemperaturAVG", "Daten":data});
    });
	GetMINMAXAVG_Daten("max","Außentemperatur",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"AußentemperaturMAX", "Daten":data});
    });


	GetMINMAXAVG_Daten("min","Innentemperatur",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"InnentemperaturMIN", "Daten":data});
    });
	GetMINMAXAVG_Daten("avg","Innentemperatur",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"InnentemperaturAVG", "Daten":data});
    });
	GetMINMAXAVG_Daten("max","Innentemperatur",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"InnentemperaturMAX", "Daten":data});
    });


	GetMINMAXAVG_Daten("min","Luftdruck",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"LuftdruckMIN", "Daten":data});
    });
	GetMINMAXAVG_Daten("avg","Luftdruck",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"LuftdruckAVG", "Daten":data});
    });
	GetMINMAXAVG_Daten("max","Luftdruck",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"LuftdruckMAX", "Daten":data});
    });


	GetMINMAXAVG_Daten("min","Luftfeuchtigkeit",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"LuftfeuchtigkeitMIN", "Daten":data});
    });
	GetMINMAXAVG_Daten("avg","Luftfeuchtigkeit",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"LuftfeuchtigkeitAVG", "Daten":data});
    });
	GetMINMAXAVG_Daten("max","Luftfeuchtigkeit",Datum24H_Früher, function(data){
        Send_to_Client({"ID":"LuftfeuchtigkeitMAX", "Daten":data});
    });
}






/*
Aufgabe:	-Holt (Min, Max oder AVG) Daten aus Datenbank
Parameter: 	-Berechnungsart: Berechnungsart(AVG, MIN, MAX)
			-Spalte:     	 Spalte der Datenbank
			-StartDatum: 	 Berchnung ab StartDatum bis jetzt
			-callback:   	 Funktion die dass Ergebniss verarbeitet(bzw. zum Client sendet)
Rückgabe:  	-keine
*/

var GetMINMAXAVG_Daten = function(Berechnungsart,Spalte,StartDatum,callback){

	con.query("SELECT " +Berechnungsart+ "("+ Spalte +") AS erg From Daten where Daten.Datum >= " + StartDatum, function (err, result, fields) {
		if(err) {
			 console.log(err);
		}
		else{		
			callback(result[0].erg);
		}
	});
}





/*
Aufgabe:	-Gibt Aktuelles Datum und Uhrzeit zurück
Parameter: 	-Keine
Rückgabe:  	-Datum
*/

function AktuellesDatum(){

	var Datum = { "Jahr":"", "Monat":"", "Tag":"", "Stunden":"", "Minuten":"", "Sekunden":"" };
	d = new Date();	
	
	Datum.Jahr = d.getYear()+1900;
	Datum.Monat = d.getMonth()+1;
	Datum.Tag = d.getDate();
	Datum.Sekunden = d.getSeconds();
	Datum.Stunden = d.getHours();
	Datum.Minuten = d.getMinutes();
	
	Datum = FormateDatum(Datum);
	return Datum;
}


/*
Aufgabe:	-Fügt 0 vor Zahlen(Tagen,Monaten) < 10
Parameter: 	-Datum
Rückgabe:  	-Geändertes Datum
*/

function FormateDatum(Datum){

	if(Datum.Monat < 10){ Datum.Monat = "0" + Datum.Monat;}
	if(Datum.Tag < 10){Datum.Tag= "0" + Datum.Tag;}
	if(Datum.Stunden < 10){ Datum.Stunden= "0" + Datum.Stunden;}
	if(Datum.Minuten < 10){ Datum.Minuten= "0" + Datum.Minuten;}
	if(Datum.Sekunden < 10){ Datum.Sekunden= "0" + Datum.Sekunden;}
	return Datum;

}


/*
Aufgabe:	-Sendet daten an alle verbundenen Clients
Parameter: 	-Nachricht an Client
Rückgabe:  	-Keine
*/

function Send_to_Client(msg) {

	var message = JSON.stringify(msg);
	
	for (var i=0; i < CLIENTS.length; i++){
		if(CLIENTS[i].readyState == CLIENTS[i].OPEN){
			CLIENTS[i].send(message);					
		}
	}
}



/*
Aufgabe:	-Warte für t ms
Parameter: 	-Wartezeit in ms
Rückgabe:  	-Keine
*/

function sleep(t) {
    var start = new Date().getTime();
    while (new Date().getTime() < start + t);
}




