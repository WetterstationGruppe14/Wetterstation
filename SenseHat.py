﻿from sense_hat import SenseHat
import time
import math
import MySQLdb

sense = SenseHat()
sense.clear()

#zum zustand switchen
index = 1

#Farben der Balken
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
yellow = (255,215,0)
violet = (85,26,139)
blank = (0,0,0)

#für höhe der balken
x=0


#Für Min/Max
temp=[]
hum=[]
press=[]
tau=[]
minT= 0
maxT= 0
minH= 0
maxH= 0
minP= 0
maxP= 0
minTau= 0
maxTau= 0

#Laufvariable für min/max
c=60


#Datenermittlung aus Sensoren
def get_Data():
  t = sense.get_temperature()
  h = sense.get_humidity()
  p = sense.get_pressure()
  
  t = round(t,1)
  h = round(h,1)
  p = round(p,1)
  
  return (t,p,h)

#Zeigt Messdaten als Laufschrift  
def moving_screen(t,p,h):
  sense.show_message('{}C {}hPa {}%'.format (t,p,h))

#Temperaturbalken
def temp_bar(t):
  	t = (t+10)/5
  	t = int(t)

 	if(-1<t<9):
   		for x in range(7,t-1,-1):
			sense.set_pixel(7, x, blank)
    		for x in range(0, t, 1):
      			sense.set_pixel(7, x, green)
  	if(t<0 or t>8):
    		for x in range(0, 8, 1):
      			sense.set_pixel(7, x, red)
#MinMaxTemperatur
def MinMaxT(t):
	t = (t+10)/5
   	t = int(t)
  
	if(len(temp) < 1440):
		temp.append(t)
	else:
		temp.append(t)
		temp.pop(0)
		
	minT=min(temp)-1
	maxT=max(temp)-1

	if(minT<0):
		minT = 0
	if(maxT<0):
		maxT = 0
	if(minT>7):
		minT = 7
	if(maxT>7):
		maxT = 7
	for x in range(8):
		sense.set_pixel(6, x, blank)

	sense.set_pixel(6, minT, green)
	sense.set_pixel(6, maxT, green)



def press_bar(p):
  p = (p-700)/50
  p = int(p)
  if(-1<p<9):
    for x in range(7, p-1, -1):
      sense.set_pixel(5, x, blank)
    for x in range(0, p, 1):
      sense.set_pixel(5, x, blue)
  else:
    for x in range(0, 8, 1):
      sense.set_pixel(5, x, red)


def MinMaxP(p):

	p = (p-700)/50
	p=int(p)

        if(len(press) < 1440):
                press.append(p)
        else:
                press.append(p)
                press.pop(0)

        minP=min(press)-1
        maxP=max(press)-1

        if(minP<0):
                minP = 0
	if(maxP<0):
		maxP = 0
	if(minP>7):
		minP = 7
        if(maxP>7):
                maxP = 7

        for x in range(8):
                sense.set_pixel(4, x, blank)

        sense.set_pixel(4, minP, blue)
        sense.set_pixel(4, maxP, blue)



def hum_bar(h):
  h = (h-10)/10
  h = int(h)
  if(-1<h<9):
    for x in range(7, h-1, -1):
      sense.set_pixel(3, x, blank)
    for x in range(0, h, 1):
      sense.set_pixel(3, x, yellow)
  else:
    for x in range(0, 8, 1):
      sense.set_pixel(3, x, red)

def MinMaxH(h):

        h = (h-10)/10
        h = int(h)

        if(len(hum) < 1440):
                hum.append(h)
        else:
                hum.append(h)
                hum.pop(0)

        minH=min(hum)-1
        maxH=max(hum)-1

        if(minH<0):
                minH = 0
	if(maxH<0):
		maxH = 0
        if(maxH>7):
                maxH = 7
	if(minH>7):
		minH = 7
        for x in range(8):
                sense.set_pixel(2, x, blank)
        sense.set_pixel(2, minH, yellow)
        sense.set_pixel(2, maxH, yellow)
def MinMaxTau(taup):


        if(len(tau) < 1440):
                tau.append(taup)
        else:
                tau.append(taup)
                tau.pop(0)

        minTau=min(tau)-1
        maxTau=max(tau)-1

        if(minTau<0):
                minTau = 0
	if(maxTau<0):
		maxTau = 0
	if(minTau>7):
		minTau = 7
        if(maxTau>7):
                maxTau = 7

        for x in range(8):
                sense.set_pixel(0, x, blank)

        sense.set_pixel(0, minTau, violet)
        sense.set_pixel(0, maxTau, violet)



def tau_bar(t,h):
  
  #Berechnung#
  k1 = 243.12
  k2 = 17.62
  
  #Sättigungsdampfdruck
  s = 6.112*(math.exp((k2*t)/(k1+t)))

  #Tatsächlicher Dampfdruck
  td = s/100*h

  #Taupunkt 
  o = td/6.112
  tau = 235*math.log(o,math.e)/(17.1-math.log(o,math.e))
  
  #Skalierung#
  tau = (tau+36)/8
  tau = int(tau)
  
  if(-1<tau<9):
    for x in range(7, tau-2, -1):
      sense.set_pixel(1, x, blank)
    for x in range(0, tau, 1):
      sense.set_pixel(1, x, violet)
  else:
    for x in range(0, 8, 1):
      sense.set_pixel(1, x, red)
  return tau


#Verbindung SQL
db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user="root",         # your username
                     passwd="123456",  # your password
                     db="Wetterstation")        # name of the data base		

cur = db.cursor()


#Checkt ob Message angzeigt werden soll. Wenn ja return diese Nachricht und löscht sie aus DB zu "Einfuegen und Speichern". 
#Wenn "Einfuegen und Speichern" zeige keine Nachricht
def checkSQL():
	cur.execute("SELECT Message from Benutzerdaten")
	db.commit()
	for row in cur.fetchall():
		if(row[0] != 'Einfuegen und Speichern'):
			message = row[0]
			cur.execute("UPDATE Benutzerdaten SET Message = 'Einfuegen und Speichern' WHERE id=1")
			db.commit()
			return (message, True)
		else: 
			return ("nothing",False)

#Checkt AT und AlarmAT. Falls unterschritten wird rote LED. Wenn nicht macht sie aus
def checkAlarm():
	
        cur.execute("SELECT Aussentemperatur from Aussentemperatur;")
        db.commit()

        for row in cur.fetchall():
                aT = row[0]

        cur.execute("SELECT Alarm_Aussen_Temp from Controll")
	db.commit()
	
	for row in cur.fetchall():
		alarm = row[0]

        if aT < alarm:
                sense.set_pixel(0, 7, red)
	if aT > alarm:
		sense.set_pixel(0, 7, blank)


z = False
##Main Loop##
while True:

	sense.set_rotation(180)
	#Zustand 1: Zeigt Balken aktueller Daten	
	if(index%2):
		(message,z) = checkSQL()
		#Falls Message dann anzeigen bis Tastendruck
		while(z):
			sense.set_rotation(0)
			sense.show_message(message)
			for event in sense.stick.get_events():
                        	if event.action == "pressed" and event.direction == "middle":
                                	sense.set_rotation(180)
					z = False
		checkAlarm()
		(t,p,h) = get_Data()
		temp_bar(t)
		press_bar(p)
		hum_bar(h)
		taup = tau_bar(t,h)
		time.sleep(1)
		
		#Aktualisiert MinMax jede Minute
		if(c >= 60):
			c = 0
			MinMaxT(t)
			MinMaxH(h)
			MinMaxP(p)
			MinMaxTau(taup)
		c = c + 1
		for event in sense.stick.get_events():
			if event.action == "pressed" and event.direction == "middle":
				index = index +1
        
	#Zustand 2: Zeigt Aktuelle Messdaten als Laufschrift
	else:
		sense.clear()
		sense.set_rotation(0)
		(message,z) = checkSQL()
                while(z):
                        sense.show_message(message)
                        for event in sense.stick.get_events():
                                if event.action == "pressed" and event.direction == "middle":
                                        z = False

		(t,p,h) = get_Data()
		moving_screen(t,p,h)

                if(c >= 60):
                        c = 0
                        MinMaxT(t)
                        MinMaxH(h)
                        MinMaxP(p)
                        MinMaxTau(taup)
		
		c = c + 8


		for event in sense.stick.get_events():
			if event.action == "pressed" and event.direction == "middle":
				index = index +1
