#ifndef CONNECT_H
#define CONNECT_H

#include "mysql++.h"

using namespace mysqlpp;

class connect
{
    public:
        static connect* getInstance();
        void connectSQL();
        Connection getConnection();


    private:
        connect();
        Connection c;
        void setConnection(Connection c);
        static connect* instance;


};

#endif // CONNECT_H
