#include "connect.h"
#include <fstream>
#include <mysql++.h>

using namespace std;
using namespace mysqlpp;

/*
Konstruktor 
*/

connect::connect()
{
    connect::connectSQL();

}

/*
Aufgabe: Setzt Connection auf ein Attribut
Parameter: Connection die zu DB 
Rückgabe: keine
*/

void connect::setConnection(Connection co)
{
    c = co;
}

/*
Aufgabe: Gibt Verbindung zurück
Parameter: Keine
Rückgabe: Verbindung
*/

Connection connect::getConnection()
{
    return c;
}




connect* connect::instance = 0;

/*
Aufgabe: Gibt Objekt zurück, falls vorhanden. Wenn nicht erzeugt ein neues
Parameter: Keine
Rückgabe: Einziges Objekt
*/

connect* connect::getInstance()
{
    if(instance == 0)
    {
        instance = new connect();
    }

    return instance;
}


/*
Aufgabe: Nimmt LoginDaten aus .txt Datei und verbindet damit mit DB
Parameter: Keine
Rückgabe: Keine. Bzw setzt Verbindung auf Attribut
*/
void connect::connectSQL()
{
   //Daten für Db
    fstream db;
    string datenbank;
    string server;
    string benutzer;
    string passwort;

    db.open("LoginDB.txt");
    getline(db, datenbank);
    getline(db, server);
    getline(db, benutzer);
    getline(db, passwort);

    mysqlpp::Connection conn(false);
    if(!conn.connect(datenbank.c_str(),server.c_str(),benutzer.c_str(),passwort.c_str())) //Datenbank,Server;Benutzer,Passwort
    {
        cout << "Connection to database failed!" << endl;
    }
    else
    {
        cout << "Connection was succesfull!" << endl;
    }

    db.close();

    connect::setConnection(conn);
}


