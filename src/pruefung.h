#ifndef PRUEFUNG_H_INCLUDED
#define PRUEFUNG_H_INCLUDED
#include "alarm.h"
#include "messung.h"
#include "sqlConfig.h"
#include "mail.h"
#include "sqlMeas.h"
#include <fstream>
#include "connect.h"

using namespace std;

class pruefung
{
    private:
        int intervallCounter;       //Counter zum schreiben in DB
        int configCounter;          //Counter zum Aktual. der Config
        int mmaCounter;             //Counter zum Aktual. der Min/Max/Avg


    public:
        pruefung();
        bool timeCheck(mail e, measure m, sqlConfig s);
        bool intervallCheck(sqlConfig s, measure m, sqlMeas q);
        bool alarmCheck(measure m, mail e, sqlConfig s, alarmexe a);
        void configCheck(sqlConfig &s);
        void incCounter();
};

#endif // PRUEFUNG_H_INCLUDED
