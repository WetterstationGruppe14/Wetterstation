#ifndef TESTPROGRAMM_H_INCLUDED
#define TESTPROGRAMM_H_INCLUDED

#include <fstream>
#include <string>

using namespace std;


class testprogramm
{

    private:

        double a;
        double i;
        int h;
        int p;

    public:
        testprogramm();
        bool testprogrammStarten();     //Ablauf des Programmes

};


#endif // TESTPROGRAMM_H_INCLUDED
