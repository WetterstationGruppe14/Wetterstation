#include "messung.h"

using namespace std;

/*
-Konstruktor
-Parameter: Daten werden direkt beim Objekt erstellen zugewiesen
*/

measure::measure(double iT, int hu, double aT, int pre )
                : iTemp( iT ), aTemp( aT ), hum( hu ), press(pre)
{


}

double measure::getiTemp()
{
    return iTemp;
}

int measure::getHum()
{
    return hum;
}

double measure::getaTemp()
{
    return aTemp;
}

int measure::getPress()
{
    return press;
}
