#ifndef SENSOREN_H
#define SENSOREN_H


class sensoren
{
    public:
        sensoren();
        bool getSensData(double &i, int &h, int &p);
        bool getATData(double &a);
};

#endif // SENSOREN_H
