#ifndef SQLCONFIG_H_INCLUDED
#define SQLCONFIG_H_INCLUDED

#include <string>

using namespace std;

class sqlConfig
{
    private:
        string email;
        string time;
        string message;
        int intervall;
        int alarmAT;

    public:
        sqlConfig();
        string getSqlEmail();
        string getSqlTime();
        string getSqlMessage();
        string getSqlIntervall();
        string getSqlAlarmAT();
        string getEmail();
        string getTime();
        string getMessage();
        int getIntervall();
        int getAlarmAT();
        void setEmail(string em);
        void setTime(string t);
        void setMessage(string m);
        void setIntervall(int i);
        void setAlarmAT(int a);
        void showData();
};

#endif // SQLCONFIG_H_INCLUDED
