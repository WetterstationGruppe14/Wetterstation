
#ifndef MAIL_H
#define MAIL_H

#include <string>
using namespace std;

class mail
{
    public:
        mail();
        bool sendMail(double iT, int hu, double aT, int pr, string adresse);
        bool sendMail(int aT, string adresse);
};

#endif // MAIL_H
