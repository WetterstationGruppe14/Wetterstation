#include "pruefung.h"
#include "alarm.h"
#include "messung.h"
#include "sqlConfig.h"
#include "sqlMeas.h"
#include <iostream>
#include <string>
#include <time.h>
#include <unistd.h>
#include <mysql++.h>
#include <sstream>
#include <time.h>
#include <cstring>
#include "connect.h"

using namespace std;

/*
Konstruktor
initialisiert config Counter = 100, sodass er am Anfang direkt configCheck ausführt
*/

pruefung::pruefung()
{
    configCounter = 100;
    intervallCounter = 0;

}

/*
-Aufgabe: Zählt alle Counter um 1 hoch
*/

void pruefung::incCounter()
{
    intervallCounter++;
    configCounter++;
    mmaCounter++;
}

/*
-Aufgabe: Prüft, ob Intervall zum Schreiben in DB erreicht wurde
-Parameter: sqlConfig zum Intervall bekommen, measure um Messwerte zu bekommen, sqlMeas zum Schreiben in Db
-Rückgabe: True wenn erfolgreich
*/

bool pruefung::intervallCheck(sqlConfig s, measure m, sqlMeas q)
{
    int intervall = s.getIntervall();


    if(intervall == 0)
    {
        cout << "Keinen Intervall angegeben" << endl << endl;
        return false;
    }
    else
    {

        if(intervallCounter >= intervall)
        {
            if(q.connectSqlMeas(m.getiTemp(), m.getHum(), m.getaTemp(), m.getPress()))
            {
                intervallCounter = 0;
                return true;

            }
            else
            {
                cout << "Fehler bei Schreiben des Objekts in DB" << endl;
                return false;
            }
        }
        else
        {
            return false;
        }


    }
}

/*
-Aufgabe: Prüft ob Außentemperatur unterschritten wurde
-Parameter: Aktuelle Messung, Mail zum Versenden der Warnung, Vom Benutzer angegebene Schwelle
-Rückgabe: Wenn erfolgreich = return true
*/

bool pruefung::alarmCheck(measure m, mail e, sqlConfig s, alarmexe a)
{
    
    int alarmAT = s.getAlarmAT();

    if(m.getaTemp() < alarmAT)
    {
        a.log(alarmAT);
        e.sendMail(alarmAT,s.getEmail());
        return true;

    }

    else
    {
        return false;
    }

}


/*
-Aufgabe: Aktualisiert Daten aus DB
-Parameter: Objekt das die Daten enthält
-Rückgabe: Objekt mit aktualisierten Daten
*/
void pruefung::configCheck(sqlConfig &s)
{
    //Sql Config Variablen
    string zeit;    //Email Zeit
    int intervall;
    int al;
    string m;       //Email
    string mess;    //Message

    //Streams
    stringstream ss; //Email Zeit
    stringstream me; //Message
    stringstream em; //Email



    if(configCounter > 10)
    {


        connect* con = connect::getInstance();
        Connection c = con->getConnection();

        //SQL Config Daten holen
        mysqlpp::Query query = c.query(s.getSqlIntervall());
        mysqlpp::StoreQueryResult res = query.store();
        intervall = res[0]["Messzeit_Intervall"];
        if(intervall != s.getIntervall())
        {
            s.setIntervall(intervall);
            cout << "Neuer Intervall gesetzt: " << intervall << endl;
        }

        query = c.query(s.getSqlTime());
        res = query.store();
        ss << res[0]["CONVERT(E_Mail_Zeit, char(8))"];
        ss >> zeit;
        if(zeit != s.getTime())
        {
            s.setTime(zeit);
            cout << "Neue Email Zeit gesetzt: " << zeit << endl;
        }


        query = c.query(s.getSqlEmail());
        res = query.store();
        em << res[0]["E_MAIL_ADDR"];
        em >> m;
        if(m != s.getEmail())
        {
            s.setEmail(m);
            cout << "Neue Email gesetzt: " << m << endl;
        }


        query = c.query(s.getSqlMessage());
        res = query.store();
        me << res[0]["Message"];
        getline(me,mess);
        if(mess != s.getMessage())
        {
            s.setMessage(mess);
            cout << "Neue Nachricht gesetzt: " << mess << endl;
        }


        query = c.query(s.getSqlAlarmAT());
        res = query.store();
        al = res[0]["Alarm_Aussen_Temp"];
        if(al != s.getAlarmAT())
        {
            s.setAlarmAT(al);
            cout << "Neue Aussentemperatur gesetzt: " << al << endl << endl;
        }


        configCounter = 0;
    }
}

/*
-Aufgabe: Überprüft ob es Zeit ist, die Email zu verschicken
-Parameter: Mail zum verschicken, messung für Daten, sqlConfig zum Uhrzeit holen
*/
bool pruefung::timeCheck(mail e, measure m, sqlConfig s)
{
    string t = s.getTime();
    string adresse = s.getEmail();
    double iT = m.getiTemp();
    int hu = m.getHum();
    double aT = m.getaTemp();
    int pr = m.getPress();

    time_t rawtime;
    time (&rawtime);
    string a = ctime (&rawtime);
    a = a.substr(11,8);


    if(t == a)
    {
        e.sendMail(iT,hu,aT,pr,adresse);
        cout << "Tägliche Email verschickt!" << endl << endl;
    }

}


