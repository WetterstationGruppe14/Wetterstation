#------------------------------------------------------------------------------#
# This makefile was generated by 'cbp2make' tool rev.147                       #
#------------------------------------------------------------------------------#


WORKDIR = `pwd`

CC = gcc
CXX = g++
AR = ar
LD = g++
WINDRES = windres

INC = -I/usr/include/ -I/usr/include/mysql++ -I/usr/include/mysql -Iinclude
CFLAGS = -std=c++11 -Wall -fexceptions
RESINC = 
LIBDIR = 
LIB = -lmysqlpp -lRTIMULib
LDFLAGS = 

INC_DEBUG = $(INC)
CFLAGS_DEBUG = $(CFLAGS) -g
RESINC_DEBUG = $(RESINC)
RCFLAGS_DEBUG = $(RCFLAGS)
LIBDIR_DEBUG = $(LIBDIR)
LIB_DEBUG = $(LIB)
LDFLAGS_DEBUG = $(LDFLAGS)
OBJDIR_DEBUG = obj/Debug
DEP_DEBUG = 
OUT_DEBUG = bin/Debug/Wetterstation

INC_RELEASE = $(INC)
CFLAGS_RELEASE = $(CFLAGS) -O2
RESINC_RELEASE = $(RESINC)
RCFLAGS_RELEASE = $(RCFLAGS)
LIBDIR_RELEASE = $(LIBDIR)
LIB_RELEASE = $(LIB)
LDFLAGS_RELEASE = $(LDFLAGS) -s
OBJDIR_RELEASE = obj/Release
DEP_RELEASE = 
OUT_RELEASE = bin/Release/Wetterstation

OBJ_DEBUG = $(OBJDIR_DEBUG)/pruefung.o $(OBJDIR_DEBUG)/testprogramm.o $(OBJDIR_DEBUG)/sqlMeas.o $(OBJDIR_DEBUG)/sqlConfig.o $(OBJDIR_DEBUG)/sensoren.o $(OBJDIR_DEBUG)/alarm.o $(OBJDIR_DEBUG)/messung.o $(OBJDIR_DEBUG)/main.o $(OBJDIR_DEBUG)/mail.o $(OBJDIR_DEBUG)/connect.o

OBJ_RELEASE = $(OBJDIR_RELEASE)/pruefung.o $(OBJDIR_RELEASE)/testprogramm.o $(OBJDIR_RELEASE)/sqlMeas.o $(OBJDIR_RELEASE)/sqlConfig.o $(OBJDIR_RELEASE)/sensoren.o $(OBJDIR_RELEASE)/alarm.o $(OBJDIR_RELEASE)/messung.o $(OBJDIR_RELEASE)/main.o $(OBJDIR_RELEASE)/mail.o $(OBJDIR_RELEASE)/connect.o

all: debug release

clean: clean_debug clean_release

before_debug: 
	test -d bin/Debug || mkdir -p bin/Debug
	test -d $(OBJDIR_DEBUG) || mkdir -p $(OBJDIR_DEBUG)

after_debug: 

debug: before_debug out_debug after_debug

out_debug: before_debug $(OBJ_DEBUG) $(DEP_DEBUG)
	$(LD) $(LIBDIR_DEBUG) -o $(OUT_DEBUG) $(OBJ_DEBUG)  $(LDFLAGS_DEBUG) $(LIB_DEBUG)

$(OBJDIR_DEBUG)/pruefung.o: pruefung.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c pruefung.cpp -o $(OBJDIR_DEBUG)/pruefung.o

$(OBJDIR_DEBUG)/testprogramm.o: testprogramm.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c testprogramm.cpp -o $(OBJDIR_DEBUG)/testprogramm.o

$(OBJDIR_DEBUG)/sqlMeas.o: sqlMeas.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c sqlMeas.cpp -o $(OBJDIR_DEBUG)/sqlMeas.o

$(OBJDIR_DEBUG)/sqlConfig.o: sqlConfig.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c sqlConfig.cpp -o $(OBJDIR_DEBUG)/sqlConfig.o

$(OBJDIR_DEBUG)/sensoren.o: sensoren.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c sensoren.cpp -o $(OBJDIR_DEBUG)/sensoren.o

$(OBJDIR_DEBUG)/alarm.o: alarm.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c alarm.cpp -o $(OBJDIR_DEBUG)/alarm.o

$(OBJDIR_DEBUG)/messung.o: messung.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c messung.cpp -o $(OBJDIR_DEBUG)/messung.o

$(OBJDIR_DEBUG)/main.o: main.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c main.cpp -o $(OBJDIR_DEBUG)/main.o

$(OBJDIR_DEBUG)/mail.o: mail.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c mail.cpp -o $(OBJDIR_DEBUG)/mail.o

$(OBJDIR_DEBUG)/connect.o: connect.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c connect.cpp -o $(OBJDIR_DEBUG)/connect.o

clean_debug: 
	rm -f $(OBJ_DEBUG) $(OUT_DEBUG)
	rm -rf bin/Debug
	rm -rf $(OBJDIR_DEBUG)

before_release: 
	test -d bin/Release || mkdir -p bin/Release
	test -d $(OBJDIR_RELEASE) || mkdir -p $(OBJDIR_RELEASE)

after_release: 

release: before_release out_release after_release

out_release: before_release $(OBJ_RELEASE) $(DEP_RELEASE)
	$(LD) $(LIBDIR_RELEASE) -o $(OUT_RELEASE) $(OBJ_RELEASE)  $(LDFLAGS_RELEASE) $(LIB_RELEASE)

$(OBJDIR_RELEASE)/pruefung.o: pruefung.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c pruefung.cpp -o $(OBJDIR_RELEASE)/pruefung.o

$(OBJDIR_RELEASE)/testprogramm.o: testprogramm.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c testprogramm.cpp -o $(OBJDIR_RELEASE)/testprogramm.o

$(OBJDIR_RELEASE)/sqlMeas.o: sqlMeas.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c sqlMeas.cpp -o $(OBJDIR_RELEASE)/sqlMeas.o

$(OBJDIR_RELEASE)/sqlConfig.o: sqlConfig.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c sqlConfig.cpp -o $(OBJDIR_RELEASE)/sqlConfig.o

$(OBJDIR_RELEASE)/sensoren.o: sensoren.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c sensoren.cpp -o $(OBJDIR_RELEASE)/sensoren.o

$(OBJDIR_RELEASE)/alarm.o: alarm.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c alarm.cpp -o $(OBJDIR_RELEASE)/alarm.o

$(OBJDIR_RELEASE)/messung.o: messung.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c messung.cpp -o $(OBJDIR_RELEASE)/messung.o

$(OBJDIR_RELEASE)/main.o: main.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c main.cpp -o $(OBJDIR_RELEASE)/main.o

$(OBJDIR_RELEASE)/mail.o: mail.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c mail.cpp -o $(OBJDIR_RELEASE)/mail.o

$(OBJDIR_RELEASE)/connect.o: connect.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c connect.cpp -o $(OBJDIR_RELEASE)/connect.o

clean_release: 
	rm -f $(OBJ_RELEASE) $(OUT_RELEASE)
	rm -rf bin/Release
	rm -rf $(OBJDIR_RELEASE)

.PHONY: before_debug after_debug clean_debug before_release after_release clean_release

