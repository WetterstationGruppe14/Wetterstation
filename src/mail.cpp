#include "mail.h"
#include "messung.h"
#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <cstring>

using namespace std;

mail::mail()
{

}

/*
-Aufgabe: Sendet Mail zu bestimmter Uhrzeit mit Daten
-Parameter: Messdaten
-Rückgabe: True wenn erfolgreich
*/

bool mail::sendMail(double iT, int hu, double aT, int pr, string adresse)
{
    string strAT = to_string(aT);
    string strIT = to_string(iT);
    string strHU = to_string(hu);
    string strPR = to_string(pr);
    string t1 = "echo ""Guten Tag, Aktuell hat es ";
    string t2 = " Grad Innentemperatur, ";
    string t3 = " Grad Außentemperatur, ";
    string t4 = " % Luftfeuchtigkeit und ";
    string t5 = " hPa Luftdruck. Mit freundlichen Gruessen, Ihre Wetterstation."" | mail -s ""Info ";
    string res = t1+strIT+t2+strAT+t3+strHU+t4+strPR+t5+adresse;
    system(res.c_str());
}

/*
-Aufgabe: Sendet Mail wenn Alarm
-Parameter: Angegebene Außentemperatur
-Rückgabe: True wenn erfolgreich
*/

bool mail::sendMail(int aT, string adresse)
{

    string strAT = to_string(aT);
    string t1 = "echo ""Guten Tag, Ihre Angegebene Aussentemperatur von ";
    string t2 = " Grad Celcius wurde unterschritten.  Mit freundlichen Gruessen, Ihre Wetterstation."" | mail -s ""Alarm ";
    string res = t1+strAT+t2+adresse;
    system(res.c_str());
}

