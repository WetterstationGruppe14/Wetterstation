#include <mysql++.h>
#include <fstream>
#include "sqlMeas.h"
#include "connect.h"

using namespace std;

sqlMeas::sqlMeas()
{

}

/*
-Aufgabe: Schreibt Daten der Messung in DB
-Parameter: Messwerte
-Rückgabe: True wenn erfolgreich
*/
bool sqlMeas::connectSqlMeas(double iT, int hu, double aT, int pr)
{
    connect* con = connect::getInstance();
    Connection c = con->getConnection();

    mysqlpp::Query query = c.query();
    mysqlpp::StoreQueryResult res = query.store();

    query << "INSERT INTO Wetterstation.Daten VALUES(" << aT << "," << iT << "," << hu << "," << pr  << "," << "NOW())";
    query.execute();


    return true;

}
