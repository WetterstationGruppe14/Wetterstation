#include "testprogramm.h"
#include "alarm.h"
#include "messung.h"
#include <iostream>
#include <fstream>
#include <mysql++.h>
#include "messung.h"
#include <unistd.h>
#include "sqlConfig.h"
#include <sstream>
#include "pruefung.h"
#include <fstream>
#include "connect.h"
#include "sensoren.h"

using namespace std;

/*
Konstruktor
*/
testprogramm::testprogramm()
{

}

/*
-Aufgabe: Beschreibt Ablauf des Programmes
          1. Öffnet .txt Dateien
          2. Holt zuerst Config Daten
          3. Erstellt Objekt Messung mit Daten aus .txt
          4. Schreibt Objekt mit Daten in DB
          5. Prüft ob Außentemperatur unterschritten wurde. Wenn ja löst Alarm aus
          6. Schleife. Siehe unten

*/
bool testprogramm::testprogrammStarten()
{
    //Objekt initialisierung
    sqlConfig s;
    pruefung pru;
    sqlMeas sm;
    mail e;
    alarmexe ae;
    connect* con = connect::getInstance();
    sensoren sens;

	 //Variable, damit Alarm nur einmal bei Unterschreitung ausgelöst wird
    bool alarmControll = false;
 
    pru.configCheck(s);
    s.showData();


    while(1)
    {


        sens.getSensData(i,h,p);
        sens.getATData(a);
        measure m(i,h,a,p);

        cout << "AT: " << a << endl;
        cout << "Objekt erstellt" << endl << endl;


        if(pru.intervallCheck(s,m,sm))
        {
            cout << "Objekt in DB geschrieben" << endl << endl;
        }

	if(!alarmControll)
	{
        	if(pru.alarmCheck(m,e,s,ae))
        	{
            		cout << "Alarm erkannt. In Log geschrieben!" << endl << endl;
			alarmControll = true;
        	}
	}
	
	if(m.getaTemp() > s.getAlarmAT())
	{
		alarmControll = false;
	}

        /*
        Schleife die pro Takt 1 Senkunde geht. So lange bis zum angegebenen Intervall auf Website.
        In jedem Takt erhöht sich Counter um 1. Uhrzeit zum Mail versenden wird pro Takt gecheckt.
        Wenn ConfigCounter hoch genug, werden Config Daten aktualisiert -> möglich im Warteintervall Intervall zu ändern
        */
        for(int i = 0; i < s.getIntervall(); i++)
        {
            sleep(1);
            pru.incCounter();
            pru.configCheck(s);
            pru.timeCheck(e,m,s);
        }
    }


}









