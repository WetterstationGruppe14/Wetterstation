#include <string>
#include "sqlConfig.h"
#include <iostream>

using namespace std;

sqlConfig::sqlConfig()
{
    email = "Nothing";
    time = "Nothing";
    message = "Nothing";
    intervall = 0;
    alarmAT = 0;


}

string sqlConfig::getSqlEmail()
{
    return "SELECT E_MAIL_ADDR FROM Wetterstation.Benutzerdaten;";
}

string sqlConfig::getSqlIntervall()
{
    return "SELECT Messzeit_Intervall FROM Wetterstation.Controll;";
}

string sqlConfig::getSqlAlarmAT()
{
    return "SELECT Alarm_Aussen_Temp FROM Wetterstation.Controll;";
}

string sqlConfig::getSqlMessage()
{
    return "SELECT Message FROM Wetterstation.Benutzerdaten;";
}

string sqlConfig::getSqlTime()
{
    return "SELECT CONVERT(E_Mail_Zeit, char(8)) FROM Wetterstation.Benutzerdaten;";
}

string sqlConfig::getEmail()
{
    return email;
}

string sqlConfig::getTime()
{
    return time;
}

string sqlConfig::getMessage()
{
    return message;
}

int sqlConfig::getIntervall()
{
    return intervall;
}

int sqlConfig::getAlarmAT()
{
    return alarmAT;
}

void sqlConfig::setEmail(string m)
{
    email = m;
}

void sqlConfig::setTime(string t)
{
    time = t;
}

void sqlConfig::setMessage(string m)
{
    message = m;
}

void sqlConfig::setIntervall(int i)
{
    intervall = i;
}

void sqlConfig::setAlarmAT(int a)
{
    alarmAT = a;
}

/*
-Aufgabe: Anzeige von den aktuellen Daten
*/

void sqlConfig::showData()
{
    cout << "Intervall: " << intervall << endl;
    cout << "Email Zeit: " << time << endl;
    cout << "Mail Adresse: " << email << endl;
    cout << "Nachricht: " << message << endl;
    cout << "Alarm Außen: " << alarmAT << endl << endl;
}
