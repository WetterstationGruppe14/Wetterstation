#include <iostream>
#include "alarm.h"
#include <mysql++.h>
#include <fstream>
#include "connect.h"

using namespace std;

/*
Konstruktor
*/

alarmexe::alarmexe()
{

}

/*
-Aufgabe: Schaltet rote LED an wenn Alarm
-Rückgabe: = True wenn erfolgreich
*/

bool alarmexe::redLED ()
{

}


/*
-Aufgabe: Schreibt Alarm in Log
-Parameter: Vom Benutzer angegebene AT
-Rückgabe: = True wenn erfolgreich
*/

void alarmexe::log (double aT)
{

    connect* con = connect::getInstance();
    Connection c = con->getConnection();


    mysqlpp::Query query = c.query();
    mysqlpp::StoreQueryResult res = query.store();

    query << "Insert into Wetterstation.Log values(" << "'Aussentemperatur von " << aT << " Grad Celsius wurde unterschritten!'" << "," << "NOW())";
    query.execute();


}


