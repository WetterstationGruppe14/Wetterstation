#ifndef MESSUNG_H_INCLUDED
#define MESSUNG_H_INCLUDED

using namespace std;

class measure
{
    private:
        double iTemp;
        int hum;
        double aTemp;
        int press;

    public:
        measure(double iT = 0, int hu = 0, double aT = 0, int pre = 0);
        double getiTemp();
        int getHum();
        double getaTemp();
        int getPress();
};
#endif // MESSUNG_H_INCLUDED
